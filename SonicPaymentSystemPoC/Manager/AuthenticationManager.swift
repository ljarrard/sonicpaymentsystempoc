import Foundation

protocol AuthenticationManagerAdapter {
  var hasLoggedIn: Bool { get set }
  var loggedIn: Bool { get }
  var loggedInDescription: String { get }
  var userId: String? { get set }
  var userProfile: AccountInfo? { get }
  func logout()
}

struct AuthenticationManagerConstants {
  static let userDefaultsUIDKey = "GigyaUID"
  static let userDefaultsHasLoggedIn = "GigyaHasLoggedIn"
  static let userDefaultsGroup = "group.com.sni.iphone.applications.ITKApp"
}

class AuthenticationManager: AuthenticationManagerAdapter {
  fileprivate typealias Constants = AuthenticationManagerConstants
  
  let gigyaAPI: GigyaAPIAdapter
  let userDefaults: UserDefaults
  let sharedUserDefaults: UserDefaults
  //let reachabilityManager: ReachabilityAdapter
  
  init(gigyaAPI: GigyaAPIAdapter = GigyaAPIManager(), userDefaults: UserDefaults = UserDefaults.standard, sharedUserDefaults: UserDefaults = UserDefaults(suiteName: Constants.userDefaultsGroup)!) {
    self.gigyaAPI = gigyaAPI
    //self.reachabilityManager = reachabilityManager
    self.userDefaults = userDefaults
    self.sharedUserDefaults = sharedUserDefaults
  }
  
  var hasLoggedIn: Bool {
    set(value) {
      if value {
        sharedUserDefaults.set(true, forKey: Constants.userDefaultsHasLoggedIn)
      }
    }
    get {
      return sharedUserDefaults.bool(forKey: Constants.userDefaultsHasLoggedIn)
    }
  }

  var loggedIn: Bool {
    return userId != nil
  }
  
  var loggedInDescription: String {
    return loggedIn ? "yes" : "no"
  }
  
  var userId: String? {
    set(id) {
      userDefaults.set(id, forKey: Constants.userDefaultsUIDKey)
      sharedUserDefaults.set(id, forKey: Constants.userDefaultsUIDKey)
      hasLoggedIn = loggedIn
    }
    get {
      guard let shareduid = sharedUserDefaults.object(forKey: Constants.userDefaultsUIDKey) as? String else {
        if let uid = userDefaults.object(forKey: Constants.userDefaultsUIDKey) as? String {
          sharedUserDefaults.set(uid, forKey: Constants.userDefaultsUIDKey)
          return uid
        }
        return nil
      }
      return shareduid
    }
  }
  
  var userProfile: AccountInfo? {
    get {
      return gigyaAPI.gigyaAccountInfo
    }
  }
  
  func logout() {
    /*
    guard reachabilityManager.networkReachable else {
      return
    }
     */
    SubscriptionManager.shared.isSubscriber = false
    gigyaAPI.logout()
    userId = nil
  }
}
