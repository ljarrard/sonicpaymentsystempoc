import Foundation
import UIKit

struct GigyaAPIConstants {
  static let underAgeCopy = "Oops! We can’t register an account for anyone 13 years or younger."
  static let loginIdentifierExistsCopy = "Hey there! This email is already registered. Please enter your password."
  static let pendingAccountNoTokenCopy = "Oops! That didn't work. Please log in again."
  static let accountLoginAPI = "accounts.login"
  static let accountInitRegistrationAPI = "accounts.initRegistration"
  static let accountRegisterAPI = "accounts.register"
  static let accountFinalizeRegistrationAPI = "accounts.finalizeRegistration"
  static let accountsGetInfoAPI = "accounts.getAccountInfo"
  static let accountSetInfoAPI = "accounts.setAccountInfo"
  static let passwordResetAPI = "accounts.resetPassword"
  static let loginIdKey = "loginID"
  static let passwordKey = "password"
  static let facebook = "facebook"
  static let googleplus = "googleplus"
  static let twitter = "twitter"
  static let yahoo = "yahoo"
  static let instagram = "instagram"
  static let existingLoginIdKey = "existingLoginID"
  static let errorKey = "error"
  static let errorCodeKey = "errorCode"
  static let canceledByUserErrorCode = 200001
  static let pendingRegistrationMessage = "Account Pending Registration"
  static let pendingRegistrationErrorCode = 206001
  static let underAgeErrorCode = 403044
  static let loginIdentifierExistsCode = 403043
  static let successErrorCode = 0
  static let UIDKey = "UID"
  static let regTokenKey = "regToken"
  static let emailKey = "email"
  static let profileKey = "profile"
  static let validationErrorsKey = "validationErrors"
  static let messageKey = "message"
  static let genderKey = "gender"
  static let ageKey = "age"
  static let birthYearKey = "birthYear"
  static let unSpecifiedGender = "u"
  static let loginProviderKey = "loginProvider"
  static let socialLoginProviderKey = "socialProviders"
  static let loginIDKey = "loginID"
  static let statusCodeKey = "statusCode"
  static let cidKey = "cid"
  static let cidValue = "ITK-iOS"
  static let regSourceKey = "regSource"
  static let regSourceValue = "ITK-iOS"
  static let userDataKey = "data"
  static let userDataValue = "{ 'mobile_app': '\(regSourceValue)' }"

  enum SocialProvider: String {
    case facebook = "facebook"
    case googleplus = "googleplus"
    case twitter = "twitter"
    case yahoo = "yahoo"
    case instagram = "instagram"
  }
}

extension GigyaAPIManager {
  typealias initResult = () throws -> (String)
  typealias accountRegisterResult = () throws -> (String)
  typealias finalizeAccountRegisterResult = () throws -> (String)
  typealias accountLoginResult = () throws -> (Bool, String?, PendingAccountInfo?, NSError?)
  typealias setAccountInfoResult = () throws -> (Bool, String?)
  typealias passwordResetResult = () throws -> (Bool)
  typealias refreshUserIdResult = () throws -> (String?)
  typealias GigyaResponse = (GSResponseAdapter?, NSError?) -> Void
}

protocol GigyaAPIAdapter {
  var gigyaAccountInfo:AccountInfo? { get }
  func logout()
  func isSessionValid() -> Bool
  func initRegistration(handler: @escaping (GigyaAPIManager.initResult) -> Void)
  func registerAccount(withEmail email: String, password: String, birthYear: String, token: String, handler: @escaping (GigyaAPIManager.accountRegisterResult) -> Void)
  func finalizeAccountRegistration(withRegistrationToken token: String, handler: @escaping (GigyaAPIManager.finalizeAccountRegisterResult) -> Void)
  func login(withEmail email: String, password: String, handler: @escaping (GigyaAPIManager.accountLoginResult) -> Void)
  func setAccountInfo(forProfile profile: PendingAccountInfo, token: String, handler: @escaping (GigyaAPIManager.setAccountInfoResult) -> Void)
  func resetPassword(withEmail email: String, handler: @escaping (GigyaAPIManager.passwordResetResult) -> Void)
  func signUp(withProvider provider: GigyaAPIConstants.SocialProvider, present over: UIViewController, handler: @escaping (GigyaAPIManager.accountLoginResult) -> Void)
  func refreshUserId(handler: @escaping (GigyaAPIManager.refreshUserIdResult) -> Void)
}


class GigyaAPIManager: GigyaAPIAdapter {
  fileprivate typealias Constants = GigyaAPIConstants

  let gigya: GigyaAdapter
  let gigyaRequestManager: GigyaGSRequestAdapter
  var gigyaAccountInfo: AccountInfo?
  
  init(gigya: GigyaAdapter = GigyaImpl(), gigyaRequestManager: GigyaGSRequestAdapter = GigyaGSRequestManager()) {
    self.gigya = gigya
    self.gigyaRequestManager = gigyaRequestManager
    self.gigyaAccountInfo = AccountInfo()
  }

  
  func initRegistration(handler: @escaping (GigyaAPIManager.initResult) -> Void) {
    gigyaRequestManager.request(withMethod: Constants.accountInitRegistrationAPI, parameters: nil) { response, error in
      if let gigyaError = error {
        handler { throw API.APIError.serverError(statusCode: gigyaError.code, message: gigyaError.localizedDescription) }
        return
      }
      guard let gigyaResponse = response else {
        handler { throw API.APIError.jsonError(nil) }
        return
      }
      guard let regToken = gigyaResponse[Constants.regTokenKey] as? String else {
        handler { throw API.APIError.jsonError(nil) }
        return
      }
      handler { return regToken }
    }
  }
  
  func registerAccount(withEmail email: String, password: String, birthYear: String, token: String, handler: @escaping (GigyaAPIManager.accountRegisterResult) -> Void) {
    let account = PendingAccountInfo(birthYear: birthYear, gender: Constants.unSpecifiedGender)
    let parameters: [String: Any] = [Constants.emailKey: email,
                                           Constants.passwordKey: password,
                                           Constants.regTokenKey: token,
                                           Constants.profileKey: account.profile,
                                           Constants.cidKey: Constants.cidValue,
                                           Constants.regSourceKey: Constants.regSourceValue,
                                           Constants.userDataKey: Constants.userDataValue]
    gigyaRequestManager.request(withMethod: Constants.accountRegisterAPI, parameters: parameters) { response, error in
      guard let gigyaResponse = response else {
        handler { throw API.APIError.jsonError(nil) }
        return
      }
      
      
      
      if let regToken = gigyaResponse[Constants.regTokenKey] as? String {
        handler { return regToken }
      }
      else if let error = gigyaResponse[Constants.validationErrorsKey] as? [AnyObject] {
        guard let errorObject = error.first as? [String: AnyObject] else {
          handler { throw API.APIError.serverError(statusCode: 400, message: "Unknown Error") }
          return
        }
        guard let message = errorObject[Constants.messageKey] as? String else {
          handler { throw API.APIError.serverError(statusCode: 400, message: "Unknown Error") }
          return
        }
        handler { throw API.APIError.serverError(statusCode: 400006, message: message) }
      } else if let errorCode = gigyaResponse[Constants.errorCodeKey] as? Int, errorCode == Constants.underAgeErrorCode {
        handler { throw API.APIError.serverError(statusCode: errorCode, message: Constants.underAgeCopy)
        }
      }
    }
  }
  
  func finalizeAccountRegistration(withRegistrationToken registrationToken: String, handler: @escaping (GigyaAPIManager.finalizeAccountRegisterResult) -> Void) {
    gigyaRequestManager.request(withMethod: Constants.accountFinalizeRegistrationAPI, parameters: [Constants.regTokenKey: registrationToken]) { response, error in
      if let gigyaError = error {
        handler { throw API.APIError.serverError(statusCode: gigyaError.code, message: gigyaError.localizedDescription) }
        return
      }
      guard let gigyaResponse = response else {
        handler { throw API.APIError.jsonError(nil) }
        return
      }
      guard let uid = gigyaResponse[Constants.UIDKey] as? String else {
        // this is to make sure that we got uid or not to verfiy the successful registration
        handler { throw API.APIError.jsonError(nil) }
        return
      }
      handler { return uid }
    }
  }
  
  func login(withEmail email: String, password: String, handler: @escaping(GigyaAPIManager.accountLoginResult) -> Void) {
    gigyaRequestManager.request(withMethod: Constants.accountLoginAPI, parameters: [Constants.loginIdKey: email, Constants.passwordKey: password]) { [weak self] response, error in
      guard let strongSelf = self else {
        return
      }
      strongSelf.handleLoginResponse(response, error: error, handler: handler)
    }
  }
  
  func setAccountInfo(forProfile profile: PendingAccountInfo, token: String, handler: @escaping (GigyaAPIManager.setAccountInfoResult) -> Void) {
    gigyaRequestManager.request(withMethod: Constants.accountSetInfoAPI, parameters: [Constants.regTokenKey: token, Constants.profileKey: profile.profile]) { response, error in
      if let error = error {
        handler { throw API.APIError.serverError(statusCode: error.code, message: error.localizedDescription) }
        return
      }
      guard let response = response , response[Constants.errorCodeKey] as? Int == Constants.successErrorCode else  {
        handler { return (false, nil) }
        return
      }
      guard let token = response[Constants.regTokenKey] as? String else {
        handler { return (true, nil) }
        return
      }
      handler { return (true, token) }
    }
  }
  
  func resetPassword(withEmail email: String, handler: @escaping (GigyaAPIManager.passwordResetResult) -> Void) {
    gigyaRequestManager.request(withMethod: Constants.passwordResetAPI, parameters: [Constants.loginIDKey: email]) { response, error in
      if let error = error {
        handler { throw API.APIError.serverError(statusCode: error.code, message: error.localizedDescription) }
        return
      }
      guard let response = response else {
        handler { return false }
        return
      }
      guard let status = response[Constants.statusCodeKey] as? Int , status == 200 else {
        handler { return false }
        return
      }
      handler { return true }
    }
  }
  
  func signUp(withProvider provider: GigyaAPIConstants.SocialProvider, present over: UIViewController, handler: @escaping (GigyaAPIManager.accountLoginResult) -> Void) {
    gigyaRequestManager.socialLogin(withProvider: provider.rawValue, over: over) { [weak self] response, error in
      guard let strongSelf = self else {
        return
      }
      strongSelf.handleLoginResponse(response, error: error, handler: handler)
    }
  }
  
  func handleLoginResponse(_ response: GSResponseAdapter?, error: NSError?, handler: @escaping (GigyaAPIManager.accountLoginResult) -> Void) {
    if gigya.isSessionValid() {
      guard let response = response, let uid = response[Constants.UIDKey] as? String else {
        handler { return (success: true, uid: nil, accountInfo: nil, error: nil) }
        return
      }
      setUserAppParameters(uid)
      if let profile = response[Constants.profileKey] as? [String: Any] {
        gigyaAccountInfo?.gender = profile[Constants.genderKey] as? String
        if let age = profile[Constants.ageKey] as? Int {
          gigyaAccountInfo?.age = String(describing: age)
        }
      } else {
        gigyaAccountInfo?.gender = response[Constants.genderKey] as? String
        gigyaAccountInfo?.age = response[Constants.ageKey] as? String
      }
      
      SubscriptionManager.shared.getSubscription(uid) { [weak self] (success) in
        handler { return (success: true, uid: uid, accountInfo: nil, error: nil) }
      }
      return
    }
    guard let error = error else {
      handler { return (success: false, uid: nil, accountInfo: nil, error: nil) }
      return
    }
    if error.code == Constants.canceledByUserErrorCode {
      handler { return (success: false, uid: nil, accountInfo: nil, error: nil) }
      return
    }    
    
    if let existingId = error.userInfo[Constants.existingLoginIdKey] as? String {
      handler { throw API.APIError.serverError(statusCode: error.code, message: existingId) }
    } else if let response = response , error.localizedDescription == Constants.pendingRegistrationMessage {
      let accountInfo = PendingAccountInfo()
      if let token = response[Constants.regTokenKey] as? String {
        accountInfo.token = token
      } else if let token = error.userInfo[Constants.regTokenKey] as? String {
        accountInfo.token = token
      }

      if let profile = response[Constants.profileKey] as? [String: Any]  {
        accountInfo.email = profile[Constants.emailKey] as? String
        accountInfo.gender = profile[Constants.genderKey] as? String
        accountInfo.birthYear = profile[Constants.birthYearKey] as? String
      } else {
        accountInfo.email = response[Constants.emailKey] as? String
        accountInfo.gender = response[Constants.genderKey] as? String
        accountInfo.birthYear = response[Constants.birthYearKey] as? String
      }
      accountInfo.loginProvider = response[Constants.loginProviderKey] as? String
      accountInfo.socialProviders = response[Constants.socialLoginProviderKey] as? String
      handler { return (success: false, uid: nil, accountInfo: accountInfo, error: error) }
    } else {
      handler { throw API.APIError.serverError(statusCode: error.code, message: error.localizedDescription) }
    }
  }
  
  func refreshUserId(handler: @escaping (GigyaAPIManager.refreshUserIdResult) -> Void) {
    guard gigya.isSessionValid() else {
      handler { return nil }
      return
    }
    gigyaRequestManager.request(withMethod: Constants.accountsGetInfoAPI, parameters: nil) { [weak self] response, error in
      if let error = error {
        handler { throw API.APIError.serverError(statusCode: error.code, message: error.localizedDescription) }
        return
      }
      guard let response = response, let userId = response[Constants.UIDKey] as? String else {
        handler { return nil }
        return
      }
      guard let strongSelf = self else {
        handler { return userId }
        return
      }
      
      if let profile = response[Constants.profileKey] as? [String: Any] {
        strongSelf.gigyaAccountInfo?.gender = profile[Constants.genderKey] as? String
        if let age = profile[Constants.ageKey] as? Int {
          strongSelf.gigyaAccountInfo?.age = String(describing: age)
        }
      } else {
        strongSelf.gigyaAccountInfo?.gender = response[Constants.genderKey] as? String
        strongSelf.gigyaAccountInfo?.age = response[Constants.ageKey] as? String
      }
      
      handler { return userId }
    }
  }
  
  func setUserAppParameters(_ uid: String) {
    let params: [String: Any] = [
      Constants.UIDKey: uid,
      Constants.cidKey: Constants.cidValue,
      Constants.regSourceKey: Constants.regSourceValue,
      Constants.userDataKey: Constants.userDataValue
    ]
    gigyaRequestManager.request(withMethod: Constants.accountSetInfoAPI, parameters: params) { _, _ in } // We don't really care about handling the response
  }
}

// MARK: Wrapper for Gigya class
extension GigyaAPIManager {
  func logout() {
    gigya.logout()
  }
  
  func isSessionValid() -> Bool {
    return gigya.isSessionValid()
  }
}

protocol GigyaGSRequestAdapter {
  func request(withMethod method: String, parameters: [String: Any]?, responseHandler: @escaping GigyaAPIManager.GigyaResponse)
  func socialLogin(withProvider provider: String, over: UIViewController, responseHandler: @escaping GigyaAPIManager.GigyaResponse)
}

class GigyaGSRequestManager: GigyaGSRequestAdapter {
  func request(withMethod method: String, parameters: [String: Any]?, responseHandler: @escaping GigyaAPIManager.GigyaResponse) {
    let request = GSRequest(forMethod: method)
    if let parameters = parameters {
      request.parameters?.setDictionary(parameters)
    }
    request.send { response, error in
      guard let response = response else {
        responseHandler(nil, error as NSError?)
        return
      }
      responseHandler(GSResponseImpl(response: response), error as NSError?)
    }
  }
  
  func socialLogin(withProvider provider: String, over: UIViewController, responseHandler: @escaping GigyaAPIManager.GigyaResponse) {
    Gigya.login(toProvider: provider, parameters: nil, over: over) { user, error in
      guard let user = user else {
        responseHandler(nil, error as NSError?)
        return
      }
      responseHandler(GSResponseImpl(response: user), error as NSError?)
    }
  }
}

struct AccountInfo {
  fileprivate typealias Constants = GigyaAPIConstants
  
  var userDefaults: UserDefaults
  
  init(userDefaults: UserDefaults = UserDefaults.standard) {
    self.userDefaults = userDefaults
  }
  
  var age:String? {
    set(id) {
      userDefaults.set(id, forKey: Constants.ageKey)
      userDefaults.synchronize()
    }
    get {
      return userDefaults.object(forKey: Constants.ageKey) as? String
    }
  }
  
  var gender:String? {
    set(id) {
      userDefaults.set(id, forKey: Constants.genderKey)
      userDefaults.synchronize()
    }
    get {
      guard let gender = userDefaults.object(forKey: Constants.genderKey) as? String else {
        return Constants.unSpecifiedGender
      }
      return gender
    }
  }
}

class PendingAccountInfo: NSObject {
  fileprivate typealias Constants = GigyaAPIConstants
  
  var email: String?
  var password: String?
  var birthYear: String?
  var gender: String?
  var token: String?
  var loginProvider: String?
  var socialProviders: String?
  
  init(email: String? = nil, password: String? = nil, birthYear: String? = nil, gender: String? = nil, token: String? = nil) {
    self.email = email
    self.password = password
    self.birthYear = birthYear
    self.gender = gender
    self.token = token
  }
  
  var profile: [String: Any] {
    var profileObject: [String: Any] = [:]
    if let email = email {
      profileObject[Constants.emailKey] = email
    }
    if let birthYear = birthYear {
      profileObject[Constants.birthYearKey] = Int(birthYear)
    }
    if let gender = gender {
      profileObject[Constants.genderKey] = gender
    }
    return profileObject
  }
}

protocol GSResponseAdapter {
  subscript(key: String) -> Any? { get }
}

class GSResponseImpl: GSResponseAdapter {
  let response: GSResponse!
  
  init(response: GSResponse) {
    self.response = response
  }
  
  subscript (key: String) -> Any? {
    return response.object(forKey: key) as Any?
  }
}

protocol GigyaAdapter {
  func logout()
  func isSessionValid() -> Bool
}

class GigyaImpl: GigyaAdapter {
  func logout() {
    Gigya.logout()
  }
  func isSessionValid() -> Bool {
    return Gigya.isSessionValid()
  }
}

