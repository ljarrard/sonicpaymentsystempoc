//
//  APIManager.swift
//  SonicPaymentSystemPoC
//
//  Created by Lauren Jarrard on 4/16/19.
//  Copyright © 2019 Discovery, Inc. All rights reserved.
//

import Foundation
import UIKit

protocol NetworkError {
  var isNetworkUnreachable: Bool { get }
}

struct API {
  static let requestIdHeader = "X-Request-ID"
  static let requestAttemptHeader = "X-Request-Attempt"
  
  fileprivate static let baseUrlQueue = DispatchQueue(label: "com.foodnetwork.api.baseUrl", attributes: .concurrent)
  fileprivate static var _baseUrl = "https://api.foodnetwork.com"
  
  static var baseUrl: String {
    get {
      var baseUrlCopy: String!
      baseUrlQueue.sync {
        baseUrlCopy = _baseUrl
      }
      return baseUrlCopy
    }
    set {
      baseUrlQueue.async(flags: .barrier) {
        _baseUrl = newValue
      }
    }
  }
  
  static var requestPlatformHeader: String {
    let deviceType = UIDevice.current.userInterfaceIdiom
    
    switch deviceType {
    case .tv:
      return "apple-tv"
    default:
      return "ios"
    }
  }
  
  static let secret = "580455eb1370031b3c23257688db0762300ee19b"
  static var sharedAuthorizationHeader: String?
  static let retryCount: UInt = 2
  
  enum APIError: Error, NetworkError {
    
    case networkUnreachableError
    case connectionError(Error?)
    case serverError(statusCode: Int, message: String?)
    case jsonError(Error?)
    case unknownError
    
    var isNetworkUnreachable: Bool {
      switch self {
      case .networkUnreachableError: return true
      default: return false
      }
    }
  }
}
