//
//  SubscriptionManager.swift
//  SonicPaymentSystemPoC
//
//  Created by Lauren Jarrard on 4/11/19.
//  Copyright © 2019 Discovery, Inc. All rights reserved.
//

import Foundation
import StoreKit

struct SubscriptionManagerConstants {
  static let allAccess = Bundle.main.bundleIdentifier! + ".sub." + "allaccess"
  static let oneWeek = Bundle.main.bundleIdentifier! + ".sub." + "monthlyaccess"
}

class SubscriptionManager: NSObject {
  static let shared = SubscriptionManager()
  
  let authenticationManager = AuthenticationManager()
  
  @objc dynamic var subscriptionOptions: [SKProduct]?
  @objc dynamic var isSubscriber: Bool = false
  
  var token: String?
  
  var canMakePayments: Bool {
    return SKPaymentQueue.canMakePayments()
  }
  
  var viewController: UIViewController?
  
  override init() {
    super.init()
    addObserver()
  }
  
  deinit {
    removeObserver()
  }
  
  func addObserver() {
    SKPaymentQueue.default().add(self)
  }
  
  func removeObserver() {
    SKPaymentQueue.default().remove(self)
  }
  
  func getSubscription(_ uid: String, handler: @escaping ((Bool) -> Void)) {
    print("get Subscription: ")
    let entitlementsUrl = URL(string: "https://api.digitalstudios.discovery.com/payments-poc-marcus/v1/entitlements")!
    var entitlementRequest = URLRequest(url: entitlementsUrl)
    entitlementRequest.httpMethod = "GET"
    entitlementRequest.addValue(uid, forHTTPHeaderField: "X-Gigya-UID")
    let task = URLSession.shared.dataTask(with: entitlementRequest) { (responseData, response, error) in
      if let error = error {
        print(error)
        handler(false)
      } else if let responseData = responseData {
        guard let json = try? JSONSerialization.jsonObject(with: responseData) as? [String: Any] else { return }
        print(json)
        let entitlements = json?["entitlements"] as? [String]
        self.isSubscriber = entitlements?.contains("fnplus") ?? false
        handler(true)
      }
    }
    task.resume()
  }
  
  func loadSubscriptions() {
    let productIDs = Set([SubscriptionManagerConstants.allAccess, SubscriptionManagerConstants.oneWeek])
    
    let request = SKProductsRequest(productIdentifiers: productIDs)
    request.delegate = self
    request.start()
  }
  
  func purchase(subscription: SKProduct) {
    let payment = SKPayment(product: subscription)
    SKPaymentQueue.default().add(payment)
  }
  
  func restorePurchases() {
    SKPaymentQueue.default().restoreCompletedTransactions()
  }
  
  func loadReceipt() -> Data? {
    guard let url = Bundle.main.appStoreReceiptURL else {
      return nil
    }
    do {
      return try Data(contentsOf: url)
    } catch {
      print("Error loading receipt data: \(error.localizedDescription)")
      return nil
    }
  }
  
  func validateReceipt(product: String) {
    guard let receiptData = loadReceipt(), let userId = authenticationManager.userId else { return }
    
    let paymentUrl = URL(string: "https://api.digitalstudios.discovery.com/payments-poc-marcus/v1/subscription/iap")!
    var paymentRequest = URLRequest(url: paymentUrl)
    paymentRequest.httpMethod = "POST"
    paymentRequest.addValue(userId, forHTTPHeaderField: "X-Gigya-UID")
    paymentRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
    paymentRequest.setValue("application/json", forHTTPHeaderField: "Accept")
    
    let body:[String: Any] = [
      "provider": "Apple",
      "receipt": receiptData.base64EncodedString(),
      "product": product,
    ]
    
    let bodyData = try! JSONSerialization.data(withJSONObject: body)
    paymentRequest.httpBody = bodyData
    
    let task = URLSession.shared.dataTask(with: paymentRequest) { (responseData, response, error) in
      if let error = error {
        print(error)
      } else if let responseData = responseData {
        print(responseData)
        guard let json = try? JSONSerialization.jsonObject(with: responseData) as? [String: Any] else { return }
        
        let data = json?["data"] as? [String: Any]
        let attributes = data?["attributes"] as? [String: Any]
        let status = attributes?["status"] as? String
        print("\(json) \(data) \(attributes) \(status)")
        if status == "ACTIVE" {
          print("sucess!!!")
        }
      }
    }

    task.resume()
  }
}

extension SubscriptionManager: SKProductsRequestDelegate {
  func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
    subscriptionOptions = response.products
  }
  
  func request(_ request: SKRequest, didFailWithError error: Error) {
    if request is SKProductsRequest {
      print("Subscription Options Failed Loading: \(error.localizedDescription)")
    }
  }
}

extension SubscriptionManager: SKPaymentTransactionObserver {
  func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
    for transaction in transactions {
      switch transaction.transactionState {
      case .purchasing:
        print("Purchasing \(transaction.payment.productIdentifier)")
      case .purchased:
        print("Purchased \(transaction.payment.productIdentifier)")
        queue.finishTransaction(transaction)
        validateReceipt(product: transaction.payment.productIdentifier)
      case .restored:
        print("Restore \(transaction.payment.productIdentifier)")
        queue.finishTransaction(transaction)
        validateReceipt(product: transaction.payment.productIdentifier)
      case .failed:
        queue.finishTransaction(transaction)
        print("Failed \(transaction.payment.productIdentifier)")
      case .deferred:
        queue.finishTransaction(transaction)
        print("Deferred \(transaction.payment.productIdentifier)")
      }
    }
  }
  
  func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error) {
    
  }
  
  func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
    
  }
}
