//
//  SubscriptionsViewController.swift
//  SonicPaymentSystemPoC
//
//  Created by Lauren Jarrard on 4/11/19.
//  Copyright © 2019 Discovery, Inc. All rights reserved.
//

import Foundation
import UIKit
import StoreKit

class SubscriptionsViewController: BaseViewController {
  
  @IBOutlet var tableView: UITableView!
  var options: [SKProduct]?
  var formatter: NumberFormatter = NumberFormatter()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    formatter.numberStyle = .currency
    formatter.formatterBehavior = .behavior10_4
    loadSubscriptions()
  }
  
  func loadSubscriptions() {
    disposeObservers()
    let subscriptionOptionsObserver = SubscriptionManager.shared.observe(\.subscriptionOptions, options: [.new, .old]) { [weak self] manager, _ in
      self?.options = SubscriptionManager.shared.subscriptionOptions
      self?.tableView.reloadData()
    }
    observers.append(subscriptionOptionsObserver)
    let subscriptionObserver = SubscriptionManager.shared.observe(\.isSubscriber, options: [.new, .old]) { [weak self] manager, _ in
      DispatchQueue.main.async {
        guard let viewController = SubscriptionManager.shared.viewController else { return }
        self?.navigationController?.popToViewController(viewController, animated: false)
      }
    }
    observers.append(subscriptionObserver)
    SubscriptionManager.shared.loadSubscriptions()
  }
}

extension SubscriptionsViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return options?.count ?? 0
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "default", for: indexPath)
    guard let option = options?[indexPath.row] else { return cell }
    cell.textLabel?.text = option.localizedTitle
    if formatter.locale != option.priceLocale {
      formatter.locale = option.priceLocale
    }
    cell.detailTextLabel?.text = formatter.string(from: option.price)
    return cell
  }
}

extension SubscriptionsViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    guard let option = options?[indexPath.row] else { return }
    
    if authenticationManager.loggedIn {
      if SubscriptionManager.shared.canMakePayments {
        SubscriptionManager.shared.purchase(subscription: option)
      } else {
        let alertController = UIAlertController(title: "Unable to Make Purchase", message: "Purchases are disabled in your device", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default))
        self.present(alertController, animated: true, completion: nil)
      }
    } else {
      let viewController = UIStoryboard(name: "NewUserViewController", bundle: .main).instantiateInitialViewController() as! NewUserViewController
      viewController.option = option
      navigationController?.pushViewController(viewController, animated: true)
    }
  }
  
  func loadContent() {
    //SubscriptionManager.shared.token = "22222"
    guard let viewController = SubscriptionManager.shared.viewController else { return }
    navigationController?.popToViewController(viewController, animated: true)
  }
}
