//
//  LoginViewController.swift
//  SonicPaymentSystemPoC
//
//  Created by Lauren Jarrard on 4/11/19.
//  Copyright © 2019 Discovery, Inc. All rights reserved.
//

import Foundation
import UIKit

class StartViewController: BaseViewController {
  @IBOutlet var stepByStepButton: UIButton!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    authenticationManager.logout()
  }
    
  @IBAction func stepByStep(_ sender: UIButton) {
    let viewController = UIStoryboard(name: "PlusPromoViewController", bundle: .main).instantiateInitialViewController() as! PlusPromoViewController
    navigationController?.pushViewController(viewController, animated: true)
  }
}
