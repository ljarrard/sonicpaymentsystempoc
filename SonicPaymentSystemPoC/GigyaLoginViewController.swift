//
//  GigyaLoginViewController.swift
//  InTheKitchen
//

import UIKit

struct GigyaLoginViewControllerConstants {
  static let socialButtonSize = CGSize(width: 40, height: 40)
  static let errorViewHeight: CGFloat = 80
  static let minimumLoginViewHeight: CGFloat = 470
  static let accessibilityEmailHint = "enter email address"
  static let accessibilityPasswordHint = "enter your password"
}

class GigyaLoginViewController: GigyaBaseViewController {
  
  @IBOutlet weak var socialButtonStackView: UIStackView!
  @IBOutlet weak var emailTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
  @IBOutlet weak var emailTextFieldInfoLabel: UILabel!
  @IBOutlet weak var passwordTextFieldInfoLabel: UILabel!
  @IBOutlet weak var emailTextFieldBorderView: UIView!
  @IBOutlet weak var passwordTextFieldBorderView: UIView!
  @IBOutlet weak var forgotPasswordButton: UIButton!
  @IBOutlet weak var logInButton: UIButton!
  @IBOutlet weak var showPasswordButton: UIButton!
  @IBOutlet weak var privacyPolicyTextView: UITextView!
  @IBOutlet weak var indicatorView: LoadingIndicatorView!
  @IBOutlet weak var contentViewBottomSpace: NSLayoutConstraint!
  var emailTickImageView = UIImageView(image: UIImage(named: "tick"))
  var viewModel: GigyaLoginViewModel = GigyaLoginViewModel()
  typealias Constants = GigyaLoginViewControllerConstants
  
  override func viewDidLoad() {
    title = "Login"
    hasTransparentNavigationBar = false
    super.viewDidLoad()
    keyBoardNavigationView.textFieldOrderArray = [emailTextField, passwordTextField]
    setUpView()
    setupSocialSignInButtonView()
    observeLogin()
  }
  
  func addCancelButton() {
    let cancelButton = UIButton(type: .custom)
    cancelButton.barItemFrame = GigyaInterstiticalViewConstants.cancelButtonFrame
    cancelButton.setTitle("Cancel", for: .normal)
    cancelButton.titleLabel?.font = viewModel.cancelButtonFont
    cancelButton.titleLabel?.adjustsFontSizeToFitWidth = true
    cancelButton.setTitleColor(.textColor, for: .normal)
    cancelButton.addTarget(self, action: .LoginControllerDismiss, for: .touchUpInside)
    navigationItem.rightBarButtonItem = UIBarButtonItem(customView: cancelButton)
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    if viewModel.initialLoad {
      view.layoutIfNeeded()
      viewModel.initialLoad = false
      if view.bounds.height < Constants.minimumLoginViewHeight {
        contentViewBottomSpace.constant = Constants.minimumLoginViewHeight - view.bounds.height
      }
    }
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.isNavigationBarHidden = false
    if let _ = presentingViewController {
      addCancelButton()
      viewModel.trackForPresentingViewController()
    }
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    if TabBarMetadata.sharedData.isTabItemSelected {
      viewModel.trackTabNav()
    }
  }
  
  override func backButton(_ sender: AnyObject?) {
    viewModel.trackBackButton()
    super.backButton(sender)
  }
  
  func setUpView() {
    logInButton.setBackgroundImage(UIImage.roundRectDarkButtonImage, for: .normal)
    
    let privacyPolicyText = privacyPolicyTextView.text
    let attributedString = NSMutableAttributedString(string: privacyPolicyText!)
    
    attributedString.addAttribute(.link, value: "terms://", range: NSString(string: privacyPolicyText ?? "").range(of: "Terms of Use"))
    attributedString.addAttribute(.link, value: "privacy://", range: NSString(string: privacyPolicyText ?? "").range(of: "Privacy Policy"))
    attributedString.addAttributes([.foregroundColor: UIColor.textColor, .font : GigyaSignUpViewControllerConstants.privacyTextFont], range: NSString(string: attributedString.string).range(of: privacyPolicyText!))
    privacyPolicyTextView.attributedText = attributedString
    privacyPolicyTextView.linkTextAttributes = [.foregroundColor: UIColor.linkColor]
    privacyPolicyTextView.textAlignment = .center
    
    emailTextField.rightViewMode = .always
    if let id = viewModel.existingMailId {
      emailTextField.text = id
    }
    
    emailTextField.accessibilityHint = Constants.accessibilityEmailHint
    passwordTextField.accessibilityHint = Constants.accessibilityPasswordHint
  }
  
  func setupSocialSignInButtonView() {
    for view in socialButtonStackView.subviews {
      view.removeFromSuperview()
    }
    guard let socialProviders = viewModel.socialProviders , !socialProviders.isEmpty else {
      return
    }
    for item in socialProviders {
      guard item != "email" else {
        continue
      }
      guard let button = viewModel.createSocialSignOnButton(item) else {
        continue
      }
      button.addTarget(self, action: #selector(GigyaLoginViewController.socialSignInProviderPressed), for: .touchUpInside)
      socialButtonStackView?.addArrangedSubview(button)
    }
  }
  
  func observeLogin() {
    let loginObserver = viewModel.observe(\.isLoggingIn, options: [.new, .old]) { [weak self] viewModel, _ in
      guard let strongSelf = self else { return }
      if viewModel.isLoggingIn {
        strongSelf.indicatorView.startAnimating()
      } else {
        strongSelf.indicatorView.stopAnimating()
      }
    }
    observers.append(loginObserver)
    let pendingAccountObserver = viewModel.observe(\.pendingAccount, options: [.new, .old]) { [weak self] viewModel, _ in
      guard let strongSelf = self, let account = viewModel.pendingAccount else { return }
      account.password = strongSelf.validPassword
      strongSelf.pushPendingRegisterView(account)
    }
    observers.append(pendingAccountObserver)
    let uidObserver = viewModel.observe(\.uid, options: [.new, .old]) { [weak self] viewModel, _ in
      guard let strongSelf = self else { return }
      if strongSelf.presentingViewController != nil {
        strongSelf.navigationController?.dismiss(animated: true, completion: nil)
      } else {
        _ = strongSelf.navigationController?.popToRootViewController(animated: true)
      }
    }
    observers.append(uidObserver)
    let errorMessageObserver = viewModel.observe(\.errorMessage, options: [.new, .old]) { [weak self] viewModel, _ in
      guard let strongSelf = self, let errorMessage = viewModel.errorMessage else { return }
      strongSelf.showErrorMessage(errorMessage)
    }
    observers.append(errorMessageObserver)
    let existingLoginIdentifierObserver = viewModel.observe(\.existingLoginIdentifier, options: [.new, .old]) { [weak self] viewModel, _ in
      guard let strongSelf = self else { return }
        strongSelf.emailTextField.text = viewModel.existingLoginIdentifier
    }
    observers.append(existingLoginIdentifierObserver)
    let forgotPasswordMessageObserver = viewModel.observe(\.forgotPasswordMessage, options: [.new, .old]) { [weak self] viewModel, _ in
      guard let strongSelf = self else { return }
      DispatchQueue.asyncAfter(delay: 0.3) {
        strongSelf.showMessage(viewModel.forgotPasswordMessage)
      }
    }
    observers.append(forgotPasswordMessageObserver)
  }
  
  func setEmailErrorState(_ message: String) {
    emailTextFieldBorderView.backgroundColor = .red
    emailTextFieldInfoLabel.text = message
    UIAccessibility.post(notification: .layoutChanged, argument: emailTextFieldInfoLabel)
    emailTextFieldInfoLabel.textColor = .red
    emailTextField.rightView = nil
  }
  
  func setEmailNormalState() {
    emailTextFieldBorderView.backgroundColor = GigyaSignUpViewControllerConstants.textFieldBorderColor
    emailTextFieldInfoLabel.text = ""
    emailTextFieldInfoLabel.textColor = GigyaSignUpViewControllerConstants.infoLabelColor
    emailTextField.rightView = emailTickImageView
  }
  
  func setPasswordErrorState(_ message: String) {
    passwordTextFieldBorderView.backgroundColor = .red
    passwordTextFieldInfoLabel.text = message
    UIAccessibility.post(notification: .layoutChanged, argument: passwordTextFieldInfoLabel)
    passwordTextFieldInfoLabel.textColor = .red
  }
  
  func setPasswordNormalState() {
    passwordTextFieldBorderView.backgroundColor = GigyaSignUpViewControllerConstants.textFieldBorderColor
    passwordTextFieldInfoLabel.text = ""
    passwordTextFieldInfoLabel.textColor = GigyaSignUpViewControllerConstants.infoLabelColor
  }
  
  func resignTextfields() {
    emailTextField.resignFirstResponder()
    passwordTextField.resignFirstResponder()
  }
  
  @objc func dismissController() {
    viewModel.trackCancel()
    self.dismiss(animated: true, completion: nil)
  }
}

//MARK: @IBAction

extension GigyaLoginViewController {
  @IBAction func login(_ sender: AnyObject) {
    resignTextfields()
    guard let email = validEmail, let password = validPassword else {
      return
    }
    viewModel.login(with: email, and: password)
  }
  
  @IBAction func forgotPassword(_ sender: AnyObject) {
    let viewController = UIStoryboard(name: "GigyaForgotPasswordViewController", bundle: .main).instantiateInitialViewController() as! GigyaForgotPasswordViewController
    viewController.loginViewModel = viewModel
    let navigationController = NavigationController(navigationBarClass: NavigationBar.self, toolbarClass: nil)
    navigationController.viewControllers = [viewController]
    present(navigationController, animated: true, completion: nil)
  }
  
  @IBAction func openPassword(_ sender: AnyObject) {
    if passwordTextField.isSecureTextEntry {
      passwordTextField.isSecureTextEntry = false
      showPasswordButton.setTitle("Hide", for: .normal)
    } else {
      passwordTextField.isSecureTextEntry = true
      showPasswordButton.setTitle("Show", for: .normal)
    }
  }
  
  @objc func socialSignInProviderPressed(_ sender: UIButton) {
    guard let buttonName = sender.restorationIdentifier else {
      return
    }
    switch buttonName {
    case "Facebook":
      facebookSignInPressed()
    case "Googleplus":
      googleplusSignInPressed()
    case "Yahoo":
      yahooSignInPressed()
    case "Twitter":
      twitterSignInPressed()
    case "Instagram":
      instagramSignInPressed()
    default:
      break
    }
  }
  
  func facebookSignInPressed() {
    viewModel.socialLogin(with: GigyaAPIConstants.SocialProvider.facebook, present: self)
  }
  
  func googleplusSignInPressed() {
    viewModel.socialLogin(with: GigyaAPIConstants.SocialProvider.googleplus, present: self)
  }
  
  func yahooSignInPressed() {
    viewModel.socialLogin(with: GigyaAPIConstants.SocialProvider.yahoo, present: self)
  }
  
  func twitterSignInPressed() {
    viewModel.socialLogin(with: GigyaAPIConstants.SocialProvider.twitter, present: self)
  }
  
  func instagramSignInPressed() {
    viewModel.socialLogin(with: GigyaAPIConstants.SocialProvider.instagram, present: self)
  }
  
  func pushPendingRegisterView(_ account: PendingAccountInfo) {
    let pendingRegisterViewController = UIStoryboard(name: "GigyaPendingRegistrationViewController", bundle: .main).instantiateInitialViewController() as! GigyaPendingRegistrationViewController
    pendingRegisterViewController.viewModel = GigyaPendingRegisterViewModel(pendingAccountInfo: account)
    navigationController?.pushViewController(pendingRegisterViewController, animated: true)
  }
  
}

// MARK: TextField delegates

extension GigyaLoginViewController: UITextFieldDelegate, UITextViewDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    // Remove the keyboard
    textField.resignFirstResponder()
    login(logInButton)
    return true
  }
  
  func textFieldDidBeginEditing(_ textField: UITextField) {
    keyBoardNavigationView.textFieldDidBeginEditing(textField)
  }
  
  func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
    if textField == emailTextField {
      _ = validEmail //check email is valid
    }
    return true
  }
  
  func textView(_ textView: UITextView, shouldInteractWith URL: Foundation.URL, in characterRange: NSRange) -> Bool {
    let scheme = URL.scheme
    if scheme == "terms" {
      openURLInAppBrowser(viewModel.termsURL)
      viewModel.trackTermsTapped("Gigya Login")
    } else if scheme == "privacy" {
      openURLInAppBrowser(viewModel.privacyPolicyURL)
      viewModel.trackPrivacyTapped("Gigya Login")
    }
    return false
  }
}

//MARK: Computed Properties
extension GigyaLoginViewController {
  var validEmail: String? {
    guard let email = emailTextField.text , !email.isEmpty else {
      setEmailErrorState(GigyaSignUpViewControllerConstants.emailRequired)
      return nil
    }
    guard email.isValidEmail() else {
      setEmailErrorState(GigyaSignUpViewControllerConstants.invalidEmail)
      return nil
    }
    setEmailNormalState()
    return email
  }
  
  var validPassword: String? {
    guard let password = passwordTextField.text , !password.isEmpty else {
      setPasswordErrorState(GigyaSignUpViewControllerConstants.passwordRequired)
      return nil
    }
    setPasswordNormalState()
    return password
  }
}

// MARK : Selectors

extension Selector {
  static let LoginControllerDismiss = #selector(GigyaLoginViewController.dismissController)
}
