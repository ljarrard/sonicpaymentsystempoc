//
//  LoginViewController.swift
//  SonicPaymentSystemPoC
//
//  Created by Lauren Jarrard on 4/11/19.
//  Copyright © 2019 Discovery, Inc. All rights reserved.
//

import Foundation
import UIKit

class LoginViewController: BaseViewController {
  @IBOutlet var usernameTextField: UITextField!
  @IBOutlet var passwordTextField: UITextField!
  @IBOutlet var loginButton: UIButton!
  
  @IBOutlet var forgotPasswordButton: UIButton!
  
  @IBOutlet var errorLabel: UILabel!
  
  let viewModel = LoginViewModel()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    observeLogin()
  }
  
  func observeLogin() {
    let errorMessageObserver = viewModel.observe(\.errorMessage, options: [.new, .old]) { [weak self] viewModel, _ in
      guard let strongSelf = self else { return }
      strongSelf.errorLabel.textColor = .red
      strongSelf.errorLabel.text = viewModel.errorMessage
    }
    observers.append(errorMessageObserver)
    let uidObserver = viewModel.observe(\.uid, options: [.new, .old]) { [weak self] viewModel, _ in
      guard let strongSelf = self else { return }
      DispatchQueue.main.async {
        strongSelf.loadContent()
      }
    }
    observers.append(uidObserver)
  }
  
  @IBAction func loginButton(_ sender: UIButton) {
    guard let email = usernameTextField.text, let password = passwordTextField.text else { return }
    viewModel.login(with: email, and: password)
  }
  
  @IBAction func forgotPasswordButton(_ sender: UIButton) {
    //this is already in the app.
  }
  
  func loadContent() {
    guard let viewController = SubscriptionManager.shared.viewController else { return }
    navigationController?.popToViewController(viewController, animated: true)
  }
}
