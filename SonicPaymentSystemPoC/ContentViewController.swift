//
//  ContentViewController.swift
//  SonicPaymentSystemPoC
//
//  Created by Lauren Jarrard on 4/11/19.
//  Copyright © 2019 Discovery, Inc. All rights reserved.
//

import Foundation
import UIKit

class ContentViewController: BaseViewController {
  @IBOutlet var logoutButton: UIButton!
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  @IBAction func logoutButton(_ sender: UIButton) {
    authenticationManager.logout()
    SubscriptionManager.shared.viewController = nil
    navigationController?.popToRootViewController(animated: true)
  }
}

