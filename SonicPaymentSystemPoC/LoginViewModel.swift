//
//  LoginViewModel.swift
//  SonicPaymentSystemPoC
//
//  Created by Lauren Jarrard on 4/16/19.
//  Copyright © 2019 Discovery, Inc. All rights reserved.
//

import Foundation

class LoginViewModel: NSObject {
  let gigyaAPIManager: GigyaAPIAdapter = GigyaAPIManager()
  var authenticationManager: AuthenticationManagerAdapter = AuthenticationManager()
  
  @objc dynamic var errorMessage: String? = nil
  @objc dynamic var isLoggingIn = false
  @objc dynamic var isLogInSuccess = false
  @objc dynamic var pendingAccount: PendingAccountInfo? = nil
  @objc dynamic var forgotPasswordMessage = ""
  @objc dynamic var uid = ""
  @objc dynamic var existingLoginIdentifier = ""
  
  func login(with email: String, and password: String) {
    isLoggingIn = true
    gigyaAPIManager.login(withEmail: email, password: password) { [weak self] result in
      guard let strongSelf = self else {
        return
      }
      strongSelf.isLoggingIn = false
      do {
        let (result: success, userId: uid, accountInfo: pendingAccount, error: _) = try result()
        strongSelf.isLogInSuccess = success
        if let uid = uid {
          strongSelf.uid = uid
          strongSelf.authenticationManager.userId = uid
          //strongSelf.trackEmailLogin(withEvent: true)
        }
        if let pendingAccount = pendingAccount {
          strongSelf.pendingAccount = pendingAccount
        }
      } catch let error {
        strongSelf.handleError(error)
      }
    }
    //trackEmailLogin(withEvent: false)
  }

  func handleError(_ error: Error) {
    switch error {
    case API.APIError.serverError(let code, let message):
      guard let message = message else {
        errorMessage = "Server Error"
        return
      }
      guard code == GigyaAPIConstants.loginIdentifierExistsCode else {
        errorMessage = message
        return
      }
      existingLoginIdentifier = message
      errorMessage = GigyaAPIConstants.loginIdentifierExistsCopy
    case API.APIError.jsonError:
      errorMessage = "Json Error"
    default:
      errorMessage = "Unknown Error"
    }
  }
}
