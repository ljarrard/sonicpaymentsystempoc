//
//  PlusPromoViewController.swift
//  SonicPaymentSystemPoC
//
//  Created by Lauren Jarrard on 4/12/19.
//  Copyright © 2019 Discovery, Inc. All rights reserved.
//

import Foundation
import UIKit

class PlusPromoViewController: BaseViewController {
  @IBOutlet var freeTrialButton: UIButton!
  @IBOutlet var signInButton: UIButton!
  @IBOutlet var restoreButton: UIButton!
  @IBOutlet var welcomeLabel: UILabel!
  @IBOutlet var imageView: UIImageView!
  @IBOutlet var logoutButton: UIButton!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    addObserver()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    reloadView()
  }
  
  func addObserver() {
    disposeObservers()
    let subscriptionObserver = SubscriptionManager.shared.observe(\.isSubscriber, options: [.new, .old]) { [weak self] manager, _ in
      DispatchQueue.main.async {
        self?.reloadView()
      }
    }
    observers.append(subscriptionObserver)
  }
  
  func reloadView() {
    if SubscriptionManager.shared.isSubscriber {
      welcomeLabel.isHidden = true
      freeTrialButton.isHidden = true
      signInButton.isHidden = true
      imageView.isHidden = false
      logoutButton.isHidden = false
    } else {
      freeTrialButton.isHidden = false
      welcomeLabel.isHidden = false
      signInButton.isHidden = authenticationManager.loggedIn
      restoreButton.isHidden = !authenticationManager.loggedIn
      imageView.isHidden = true
      logoutButton.isHidden = true
    }
  }
  
  @IBAction func freeTrialButton(_ sender: UIButton) {
    SubscriptionManager.shared.viewController = self
    
    let viewController = UIStoryboard(name: "SubscriptionsViewController", bundle: .main).instantiateInitialViewController() as! SubscriptionsViewController
    navigationController?.pushViewController(viewController, animated: true)
  }
  
  @IBAction func restoreButton(_ sender: UIButton) {
    SubscriptionManager.shared.restorePurchases()
  }
  
  @IBAction func signInButton(_ sender: UIButton) {
    SubscriptionManager.shared.viewController = self
    
    let viewController = UIStoryboard(name: "LoginViewController", bundle: .main).instantiateInitialViewController() as! LoginViewController
    navigationController?.pushViewController(viewController, animated: true)
  }
  
  @IBAction func logoutButton(_ sender: UIButton) {
    authenticationManager.logout()
    SubscriptionManager.shared.viewController = nil
  }
}
