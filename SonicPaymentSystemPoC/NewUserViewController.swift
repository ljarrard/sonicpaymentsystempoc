//
//  NewUserViewController.swift
//  SonicPaymentSystemPoC
//
//  Created by Lauren Jarrard on 4/11/19.
//  Copyright © 2019 Discovery, Inc. All rights reserved.
//

import Foundation
import UIKit
import StoreKit

class NewUserViewController: BaseViewController {
  @IBOutlet var emailTextField: UITextField!
  @IBOutlet var passwordTextField: UITextField!
  @IBOutlet var birthYearTextField: UITextField!
  @IBOutlet var signUpButton: UIButton!
  
  @IBOutlet var errorLabel: UILabel!
  
  var option: SKProduct?
  
  let viewModel = NewUserViewModel()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    addObservers()
  }
  
  func addObservers() {
    let errorMessageObserver = viewModel.observe(\.errorMessage, options: [.new, .old]) { [weak self] viewModel, _ in
      guard let strongSelf = self else { return }
      strongSelf.errorLabel.textColor = .red
      strongSelf.errorLabel.text = viewModel.errorMessage
    }
    observers.append(errorMessageObserver)
    let uidObserver = viewModel.observe(\.uid, options: [.new, .old]) { [weak self] viewModel, _ in
      guard let strongSelf = self else { return }
      DispatchQueue.main.async {
        strongSelf.buySubscription()
      }
    }
    observers.append(uidObserver)
  }
  
  @IBAction func signUpButton(_ sender: UIButton) {
    viewModel.signUp{ [weak self] registerInput -> Bool in
      guard let strongSelf = self else {
        return false
      }
      registerInput.email = strongSelf.emailTextField.text
      registerInput.password = strongSelf.passwordTextField.text
      registerInput.birthYear = strongSelf.birthYearTextField.text
      return true
    }
  }
  
  func buySubscription() {
    guard let option = option else { return }
    SubscriptionManager.shared.purchase(subscription: option)
  }
  
  func loadContent() {
    guard let viewController = SubscriptionManager.shared.viewController else { return }
    navigationController?.popToViewController(viewController, animated: true)
  }
}
