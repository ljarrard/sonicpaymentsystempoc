//
//  SignInOptionsViewController.swift
//  SonicPaymentSystemPoC
//
//  Created by Lauren Jarrard on 4/12/19.
//  Copyright © 2019 Discovery, Inc. All rights reserved.
//

import Foundation
import UIKit

class SignInOptionsViewController: BaseViewController {
  @IBOutlet var signUpButton: UIButton!
  @IBOutlet var loginButton: UIButton!
  
  @IBAction func signUpButton(_ sender: UIButton) {
  
  }
  
  @IBAction func loginButton(_ sender: UIButton) {
  
  }
}
