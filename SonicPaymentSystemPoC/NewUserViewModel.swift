
class NewUserViewModel: NSObject {
  
  var authenticationManager: AuthenticationManagerAdapter = AuthenticationManager()
  let gigyaAPIManager: GigyaAPIAdapter = GigyaAPIManager()
  
  var initRegToken: String? = nil
  @objc dynamic var errorMessage = ""
  @objc dynamic var uid = ""
  typealias inputHandler = (RegisterInput) -> Bool
  @objc dynamic var isRegistering = false
  var isPasswordFieldBecomeResponder = false
  var isInitialLoad = true
  @objc dynamic var isLoggingIn = false
  @objc dynamic var isLogInSuccess = false
  
  var input = RegisterInput()
  
  func signUp(_ handler: @escaping inputHandler) {
    errorMessage = ""
    guard let regToken = initRegToken else {
      initAccountRegistration(handler)
      return
    }
    accountRegister(regToken, handler: handler)
  }
  
  func initAccountRegistration(_ handler: @escaping inputHandler) {
    isRegistering = true
    //trackSignUp()
    gigyaAPIManager.initRegistration { [weak self] result in
      guard let strongSelf = self else {
        return
      }
      do {
        let token = try result()
        strongSelf.initRegToken = token
        strongSelf.accountRegister(token, handler: handler)
      } catch let error {
        strongSelf.isRegistering = false
        strongSelf.handleError(error)
      }
    }
  }
  
  func accountRegister(_ regToken: String, handler: inputHandler) {
    input = RegisterInput()
    guard handler(input) else {
      isRegistering = false
      return
    }
    
    gigyaAPIManager.registerAccount(withEmail: input.email, password: input.password, birthYear: input.birthYear, token: regToken) { [weak self] result in
      guard let strongSelf = self else {
        return
      }
      do {
        let token = try result()
        strongSelf.finalizeAccountRegistration(token)
      } catch let error {
        strongSelf.isRegistering = false
        strongSelf.handleError(error)
      }
    }
  }
  
  func finalizeAccountRegistration(_ regToken: String) {
    gigyaAPIManager.finalizeAccountRegistration(withRegistrationToken: regToken) { [weak self] result in
      guard let strongSelf = self else {
        return
      }
      strongSelf.isRegistering = false
      do {
        let uid = try result()
        //strongSelf.trackSuccessfulSignUp()
        strongSelf.getUserInfo()
        strongSelf.uid = uid
        strongSelf.authenticationManager.userId = uid
      } catch let error {
        strongSelf.handleError(error)
      }
    }
  }
  
  func getUserInfo() {
    if !gigyaAPIManager.isSessionValid() {
      if let email = input.email, let password = input.password {
        login(with: email, and: password)
      }
    } else {
      gigyaAPIManager.refreshUserId() { _ in
      }
    }
  }
  
  func login(with email: String, and password: String) {
    isLoggingIn = true
    gigyaAPIManager.login(withEmail: email, password: password) { [weak self] result in
      guard let strongSelf = self else {
        return
      }
      strongSelf.isLoggingIn = false
      do {
        let (result: success, userId: uid, accountInfo: _, error: _) = try result()
        strongSelf.isLogInSuccess = success
        if let uid = uid {
          strongSelf.uid = uid
          strongSelf.authenticationManager.userId = uid
          //strongSelf.trackEmailLogin(withEvent: true)
        }
      } catch let error {
        strongSelf.handleError(error)
      }
    }
    //trackEmailLogin(withEvent: false)
  }
  
  func handleError(_ error: Error) {
    switch error {
    case API.APIError.serverError(_, let message):
      guard let message = message else {
        errorMessage = "Server Error"
        return
      }
      errorMessage = message
    case API.APIError.jsonError:
      errorMessage = "Json Error"
    default:
      errorMessage = "Unknown Error"
    }
  }
}

class RegisterInput {
  var email: String!
  var password: String!
  var birthYear: String!
}
