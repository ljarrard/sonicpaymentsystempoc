//
//  BaseViewController.swift
//  SonicPaymentSystemPoC
//
//  Created by Lauren Jarrard on 4/11/19.
//  Copyright © 2019 Discovery, Inc. All rights reserved.
//

import Foundation
import UIKit

class BaseViewController: UIViewController {
  var observers: [NSKeyValueObservation] = []
  
  let authenticationManager = AuthenticationManager()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    /*
    let editButton = UIButton(type: .custom)
    editButton.setTitle("LogOut", for: .normal)
    editButton.titleLabel?.adjustsFontSizeToFitWidth = true
    editButton.titleLabel?.lineBreakMode = .byClipping
    editButton.addTarget(self, action: #selector(logout(_:)), for: .touchUpInside)
    navigationItem.rightBarButtonItem = UIBarButtonItem(customView: editButton)
    */
  }
  
  /*
  @objc func logout(_ sender: UIButton) {
    authenticationManager.logout()
  }
 */
  
  func disposeObservers() {
    observers.forEach { $0.invalidate() }
    observers = []
  }
}
